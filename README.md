# Contao Yellow Pages Bundle

## About
Gewerbeverzeichnis für die Gemeinde Lösung

### Features
- [x] Gewerbeverzeichnis mit Kategorien
- [x] Erweiterung der Standard Member Tabelle 


## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "git",
    "url" : "https://bitbucket.org/memo_development/contao-yellow-pages-bundle"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-yellow-pages-bundle": "dev-master",
```

## Usage (German)
1. Memo Yellow Pages Bundle installieren



## Contribution
Bug reports and pull requests are welcome

