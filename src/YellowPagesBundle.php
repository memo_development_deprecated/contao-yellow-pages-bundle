<?php
/**
 * @package   YellowPagesBundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\YellowPagesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;


class YellowPagesBundle extends Bundle
{

}
