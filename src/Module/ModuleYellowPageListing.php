<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\YellowPagesBundle\Module;

use Memo\CategoryBundle\Model\CategoryModel;
use Memo\YellowPagesBundle\Model\YellowPagesEntryModel;

class ModuleYellowPageListing extends \Module
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_yellow_page_listing';

    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=tl_yellow_pages"><strong>hier verwalteten Inhalte</strong></a>.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }

    protected function compile()
    {
        $limit  = null;
        $offset = 0;

        // Maximum number of items
        if ($this->numberOfItems > 0)
        {
            $limit = $this->numberOfItems;
        }

        $this->Template->articles = array();
        $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];

        // Get the total number of items
        $intTotal = $this->countItems(unserialize($this->yellow_pages_archives));

        if ($intTotal < 1)
        {
            return;
        }

        $total = $intTotal - $offset;

        // Split the results
        if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
        {
            // Adjust the overall limit
            if (isset($limit))
            {
                $total = min($limit, $total);
            }

            // Get the current page
            $id = 'page_n' . $this->id;
            $page = Input::get($id) ?? 1;

            // Do not index or cache the page if the page number is outside the range
            if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
            {
                throw new PageNotFoundException('Page not found: ' . Environment::get('uri'));
            }

            // Set limit and offset
            $limit = $this->perPage;
            $offset += (max($page, 1) - 1) * $this->perPage;
            $skip = (int) $this->skipFirst;

            // Overall limit
            if ($offset + $limit > $total + $skip)
            {
                $limit = $total + $skip - $offset;
            }

            // Add the pagination menu
            $objPagination = new Pagination($total, $this->perPage, Config::get('maxPaginationLinks'), $id);
            $this->Template->pagination = $objPagination->generate("\n  ");
        }

        $objArticles = $this->fetchItems(unserialize($this->yellow_pages_archives), ($limit ?: 0), $offset);

        // Add the articles
        if ($objArticles !== null)
        {
            $this->Template->articles = $this->parseArticles($objArticles);
        }
    }

    /**
     * Fetch the matching items
     *
     * @param array   $aYelloPagesArchives
     * @param integer $limit
     * @param integer $offset
     *
     * @return Collection|YellowPageEntryModel|null
     */
    protected function fetchItems(array $aYelloPagesArchives, int $limit, int $offset)
    {

        // Determine sorting
        $t = YellowPagesEntryModel::getTable();
        $order = '';

        switch ($this->yellow_pages_order)
        {
            case 'order_company_asc':
                $order .= "$t.company";
                break;

            case 'order_company_desc':
                $order .= "$t.company DESC";
                break;

            case 'order_random':
                $order .= "RAND()";
                break;

            case 'order_dateAdded_asc':
                $order .= "$t.dateAdded";
                break;

            default:
                $order .= "$t.dateAdded DESC";
        }

        return YellowPagesEntryModel::findPublishedByPids($aYelloPagesArchives, $limit, $offset, array('order'=>$order));
    }


    /**
     * Parse one or more items and return them as array
     *
     * @param Collection $objArticles
     * @param boolean    $blnAddArchive
     *
     * @return array
     */
    protected function parseArticles($objArticles, $blnAddArchive=false)
    {
        $limit = $objArticles->count();

        if ($limit < 1)
        {
            return array();
        }

        $count = 0;
        $arrArticles = array();
        $uuids = array();

        foreach ($objArticles as $objArticle)
        {
            if ($objArticle->addImage && $objArticle->singleSRC)
            {
                $uuids[] = $objArticle->singleSRC;
            }
        }

        // Preload all images in one query, so they are loaded into the model registry
        \FilesModel::findMultipleByUuids($uuids);

        foreach ($objArticles as $objArticle)
        {
            $arrArticles[] = $this->parseArticle($objArticle, $blnAddArchive, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count);
        }

        return $arrArticles;
    }

    /**
     * Parse an item and return it as string
     *
     * @param NewsModel $objArticle
     * @param boolean   $blnAddArchive
     * @param string    $strClass
     * @param integer   $intCount
     *
     * @return string
     */
    protected function parseArticle($objArticle, $blnAddArchive=false, $strClass='', $intCount=0)
    {
        $objTemplate = new \FrontendTemplate($this->yellow_pages_template ?: 'yellow_pages_listitem');
        $objTemplate->setData($objArticle->row());

        if ($objArticle->cssClass)
        {
            $strClass = ' ' . $objArticle->cssClass . $strClass;
        }

        if ($objArticle->featured)
        {
            $strClass = ' featured' . $strClass;
        }

        $objTemplate->class = $strClass;


        // Clean the RTE output
        $objTemplate->description = \StringUtil::toHtml5($objArticle->description);
        $objTemplate->description = \StringUtil::encodeEmail($objTemplate->description);
        $objTemplate->description = \StringUtil::decodeEntities($objTemplate->description);

        $objTemplate->timestamp = $objArticle->date;
        $objTemplate->datetime = date('Y-m-d\TH:i:sP', $objArticle->date);

        // Add an image
        $objModel = \FilesModel::findByUuid($objArticle->singleSRC);
        if ($objModel !== null && is_file(\System::getContainer()->getParameter('kernel.project_dir') . '/' . $objModel->path))
        {
            // Do not override the field now that we have a model registry (see #6303)
            $arrArticle = $objArticle->row();

            // Override the default image size
            if ($this->imgSize)
            {
                $size = \StringUtil::deserialize($this->imgSize);

                if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]) || ($size[2][0] ?? null) === '_')
                {
                    $arrArticle['size'] = $this->imgSize;
                }
            }

            $arrArticle['singleSRC'] = $objModel->path;
            $this->addImageToTemplate($objTemplate, $arrArticle, null, null, $objModel);

            // Link to the news article if no image link has been defined (see #30)
            if (!$objTemplate->fullsize && !$objTemplate->imageUrl)
            {
                // Unset the image title attribute
                $picture = $objTemplate->picture;
                unset($picture['title']);
                $objTemplate->picture = $picture;

                // Link to the news article
                $objTemplate->href = $objTemplate->link;
                $objTemplate->linkTitle = \StringUtil::specialchars(sprintf($GLOBALS['TL_LANG']['MSC']['readMore'], $objArticle->headline), true);

                // If the external link is opened in a new window, open the image link in a new window, too (see #210)
                if ($objTemplate->source == 'external' && $objTemplate->target && strpos($objTemplate->attributes, 'target="_blank"') === false)
                {
                    $objTemplate->attributes .= ' target="_blank"';
                }
            }
        }
        return $objTemplate->parse();
    }


    /**
     * @param array|null $arrYellowPagesArchives
     * @return int
     */
    protected function countItems(array $arrYellowPagesArchives)
    {
        if(is_null($arrYellowPagesArchives)) {
            return 0;
        }
        return YellowPagesEntryModel::countPublishedByPids($arrYellowPagesArchives);
    }
}
