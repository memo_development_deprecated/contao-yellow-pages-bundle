<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\YellowPagesBundle\Module;

use Contao\FrontendUser;
use Memo\CategoryBundle\Model\CategoryModel;


class ModuleYellowPageKategorieFilter extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_yellow_page_categorie_filter';

    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=calendar&table=tl_calendar_events&id=1"><strong>hier verwalteten Inhalte</strong></a>.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }

    protected function compile()
    {
        $aCategories = unserialize($this->categories);
        $aCat = [];
        $oCategories = CategoryModel::findAll(['order'=>'title ASC']);

        foreach($oCategories as $key => $val)
        {
            if(in_array($val->id,$aCategories) OR in_array($val->pid,$aCategories)) {
                $aCat[] = $val;
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->categories = $aCat;
    }
}
