<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\YellowPagesBundle\Module;

use Contao\FrontendUser;
use Memo\CategoryBundle\Model\CategoryModel;
use Memo\CategoryBundle\Service\CategoryService;
use Memo\YellowPagesBundle\Model\YellowPagesEntryModel;


class ModuleYellowPageRegisterForm extends \Module
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_yellow_page_register_form';

	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \Contao\BackendTemplate('be_wildcard');
			$objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=tl_yellow_pages&table=tl_yellow_pages_entry&id=1"><strong>hier verwalteten Inhalte</strong></a>.';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;

			return $objTemplate->parse();
		}
		return parent::generate();
	}

	protected function compile()
	{

		$this->Template->form = null;
		$this->Template->blnLoggedIn = true;
        $objUser        = FrontendUser::getInstance();
        $aAllowedGroups = unserialize($this->groups);

		if (TL_MODE === 'FE')
		{
            $blnAccessAllowed = true;
            if(!empty($aAllowedGroups) && $this->protected){
                $blnAccessAllowed = false;
                foreach($objUser->groups as $key => $val)
                {
                   if(in_array($val,$aAllowedGroups)) {
                       $blnAccessAllowed = true;
                       break;
                   }
                }
            }

			if (FE_USER_LOGGED_IN === true && $blnAccessAllowed === true) {

				$objForm = new \Haste\Form\Form('createEventItem', 'POST', function ($objHaste) {
					return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
				});

                //Get Values | edit Item
                $oData  = null;
                $selItem = null;

                $prmItem = \Input::get('item');

                if(!is_null($prmItem))
                {
                    //get Values
                    $oTmp   = YellowPagesEntryModel::findByPk(intval($prmItem));
                    $oAllowedMembers = unserialize($oTmp->memberid);

                    //have access to edit
                    if(is_array($oAllowedMembers) AND in_array($objUser->id,$oAllowedMembers)) {
                        $oData = $oTmp;
                    }
                    if(!empty($oData->categories)) {
                        $selItem = unserialize($oData->categories)[0];
                   }
                }else{
                    /**
                     * User Element
                     * @todo List Items if more than one relation
                     */
                    $oData   = YellowPagesEntryModel::findOneBy('memberid',$objUser->id);
                    if(!empty($oData->categories))
                    {
                        $selItem = unserialize($oData->categories)[0];
                    }
                }

                //add categorie id
                $aEventCat = CategoryService::getAllCategories(true,unserialize($this->categorieGroups));

                $objForm->addFormField('categories', array(
                    'name' => 'categorie',
                    'label' => 'Kategorie',
                    'inputType' => 'checkbox',
                    'ignoreModelValue' => true,
                    'value' => $selItem,
                    'options' => $aEventCat
                ));


                //allowed fields 4 form
                \Controller::loadDataContainer('tl_yellow_pages_entry');
                \System::loadLanguageFile('tl_yellow_pages_entry');
                $uploadFolder = \FilesModel::findByUuid($this->uploadDir);

                $aFields = unserialize($this->formFields);
                foreach($aFields as $key => $field) {
                    //Input type File
                    if($GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['inputType'] === 'fileTree') {
                        $objForm->addFormField($field, array(
                            'label'         => $GLOBALS['TL_LANG']['tl_yellow_pages_entry'][$field][0],
                            'inputType'     => 'upload',
                            'eval'          => array('extensions'=>'jpg,jpeg,gif,png', 'storeFile'=>true, 'uploadFolder'=> $uploadFolder->uuid, 'doNotOverwrite' => true, 'maxlength' => 2048000)
                        ));
                    }else{
                        //other Fields
                        if (!is_null($oData)) {
                            //add current Data if oData not null
                            $GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['value'] = $oData->$field;
                        }
                        $GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields']['label'] = $GLOBALS['TL_LANG']['tl_yellow_pages_entry'][$field][0];
                        $objForm->addFormField($field, $GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]);
                    }
                }

                //Add record ID 4 edit
                if(!is_null($prmItem)) {
					$objForm->addFormField('itemID', array(
			            'name' => 'id',
			            'inputType' => 'hidden',
			            'ignoreModelValue' => true,
			            'value' => $prmItem
			        ));
				}

				$objForm->addSubmitFormField('submit', $this->buttonLabel);
				$objForm->addCaptchaFormField('captcha');

                $blnNew = false;
                $arrTokens = [];
				if ($objForm->validate()) {

					// Get all the submitted and parsed data (only works with POST):
					$arrData = $objForm->fetchAll();

					$prmItemID = \Input::post('id');
					if(empty($prmItemID))
					{
						//Create New Yellow Page Entry
						$objYpe = new YellowPagesEntryModel();
						$objYpe->disable    = null;
						$objYpe->memberid 	= serialize([$objUser->id]);
						$objYpe->pid		= $this->yellowPageArchive;
						$objYpe->tstamp 	= time();
						$objYpe->dateAdded 	= time();
                        $blnNew = true;

					}else{
						$objYpe = YellowPagesEntryModel::findByPk(intval($prmItemID));
					}

                    //dynamic add fields from form
                    foreach($aFields as $key => $field) {

                        if(!is_array($field)) {
                            //add Uploaded Files
                            if ($GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['inputType'] == 'fileTree') {
                                if ($_SESSION['FILES'][$field]['name']) {
                                    $objYpe->singleSRC = \Dbafs::addResource($uploadFolder->path . "/" . $_SESSION['FILES'][$field]['name'])->uuid;
                                    $arrTokens['form_image'] = $uploadFolder->path . "/" . $_SESSION['FILES'][$field]['name'];
                                }
                            } elseif (isset($GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['eval']['rgxp']) and $GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['eval']['rgxp'] == 'datim' or $GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['eval']['rgxp'] == 'date') {
                                //convert Date format
                                $objYpe->$field = strtotime(trim($arrData[$field]));

                                //Add Tokens for Notification
                                $arrTokens['form_' . $field] = date("d.m.Y", $objYpe->$field);

                            } else {
                                //save other Fields
                                $objYpe->$field = trim($arrData[$field]);

                                //Add Tokens for Notification
                                $arrTokens['form_' . $field] = trim($arrData[$field]);
                                if ($GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'][$field]['inputType'] === 'textarea') {
                                    //Add HTML Format Token
                                    $arrTokens['form_' . $field . '_html'] = \StringUtil::decodeEntities(trim($arrData[$field]));
                                }
                            }
                        }

                    }

                    //Add Categories
                    $aCategories = \Input::post('categorie');

                    if(is_array($aCategories)) {
                        $objYpe->categories = serialize($aCategories);
                    }else{
                        $objYpe->categories = serialize([$aCategories]);
                    }

                    if(!empty($aCategories)) {
                        $arrTokens['form_categories'] = implode(", ", $aCategories);
                    }else{
                        $arrTokens['form_categories'] = '';
                    }
                    //Save Record
					$objYpe->save();

                    //Send Notification
                    if(!empty($this->notification) and is_numeric($this->notification)) {
                        $oNotification = \NotificationCenter\Model\Notification::findByPk($this->notification);
                        if (null !== $oNotification) {
                            $oNotification->send($arrTokens);
                        }
                    }

                    //Redirect to OK-Page
                    if($this->jumpTo) {
                        $urlGenerator = \System::getContainer()->get('contao.routing.url_generator');
                        $oOkPage = \PageModel::findById($this->jumpTo);
                        \Controller::redirect($urlGenerator->generate($oOkPage->alias));
                    }
				}


				// Get the form as string
				$this->Template->form = $objForm->generate();
			}else{
				$this->Template->blnLoggedIn = false;
                //Do Redirect on Login - Page
                if($this->loginRedirect) {
                    $urlGenerator = \System::getContainer()->get('contao.routing.url_generator');
                    $loginPage = \PageModel::findById($this->loginRedirect);
                    \Controller::redirect($urlGenerator->generate($loginPage->alias));
                }
				\System::log("Create FE YellowPage view not allowed",__METHOD__,TL_ACCESS);
			}
			$this->Template->headline = $this->headline;

		}
	}
}
