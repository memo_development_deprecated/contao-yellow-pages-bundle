<?php


$GLOBALS['TL_LANG']['tl_module']['categories_legend']   = 'Gewerbeverzeichnis Kategorien';
$GLOBALS['TL_LANG']['tl_module']['formfields_legend']   = 'Formularfelder';
$GLOBALS['TL_LANG']['tl_module']['button_legend']		= 'Button Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['notification']		= ['Benachrichtigung wählen','Über welche Benachrichtigung soll die Anfrage versendet werden?'];
$GLOBALS['TL_LANG']['tl_module']['categorieGroups']		= ['Kategorien wählen',''];
$GLOBALS['TL_LANG']['tl_module']['formFields']		    = ['Formularfelder',''];
$GLOBALS['TL_LANG']['tl_module']['yellowPageArchive']		    = ['Gewerbeverzeichnis Archiv','Welchem Archiv sollen die Einträge zugeordnet werden?'];
$GLOBALS['TL_LANG']['tl_module']['uploadDir']		    = ['Uplaod Verzeichnis','Wo sollen die Daten hochgeladen werden?'];
$GLOBALS['TL_LANG']['tl_module']['loginRedirect']		    = ['Login Redirect','Redirect wenn nicht eingeloggt?'];
$GLOBALS['TL_LANG']['tl_module']['buttonLabel']		    = ['Absenden Button Text','Welcher Text soll auf dem Absenden Button sein?'];
$GLOBALS['TL_LANG']['tl_module']['form_legend']		    = 'Formular Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['yellow_pages_archives']		    = ['Quellverzeichnisse'];
$GLOBALS['TL_LANG']['tl_module']['yellow_pages_order']		    = ['Listen-Sortierung'];
$GLOBALS['TL_LANG']['tl_module']['yellow_pages_template']		    = ['Listen-Template'];
$GLOBALS['TL_LANG']['tl_module']['order_company_asc']		    = 'Firma A-Z';
$GLOBALS['TL_LANG']['tl_module']['order_company_desc']		    = 'Firma Z-A';
$GLOBALS['TL_LANG']['tl_module']['order_dateAdded_asc']		    = 'Registrierungsdatum 0-9';
$GLOBALS['TL_LANG']['tl_module']['order_dateAdded_desc']		    = 'Registrierungsdatum 9-0';
$GLOBALS['TL_LANG']['tl_module']['yellow_pages_register_link']		    = ['Button Link (FE Login)'];
$GLOBALS['TL_LANG']['tl_module']['yellow_pages_register_buttonLabel']		    = ['Button Label (FE Login)'];
