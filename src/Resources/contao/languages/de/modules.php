<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['tl_yellow_pages'] = ['Verzeichnisse',''];
$GLOBALS['TL_LANG']['MOD']['memo_yellowpages'] = ['Gewerbeverzeichnis',''];

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['memo_yellowpages'] = ['MEMO Gewerbeverzeichnis',''];
$GLOBALS['TL_LANG']['FMD']['memo_yellowpages_register_form'] = ['Gewerbeverzeichnis-Formular (Eingabe)',''];
$GLOBALS['TL_LANG']['FMD']['memo_yellowpages_categorie_filter'] = ['Gewerbeverzeichnis-Kategorie Filter',''];
$GLOBALS['TL_LANG']['FMD']['memo_yellowpages_listing']		    = ['Gewerbeverzeichnis Listing'];

