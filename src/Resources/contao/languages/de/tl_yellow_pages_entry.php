<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['categories_legend']   = 'Kategorien';
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['address_legend']      = 'Adresse';
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['contact_legend']      = 'Kontakt Person';
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['member_legend']       = 'Mitglieder Zuordnung';
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['account_legend']      = 'Kontoeinstellungen';

$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['categories']   = ['Kategorie Zuordnung',"Welche Kategorien sollen zur Verfügung stehen?"];

$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['company']     = ['Firma',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['street']      = ['Strasse',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['postal']      = ['Postleitzahl',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['city']        = ['Ort',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['state']       = ['Staat',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['country']     = ['Land',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['description']     = ['Beschreibung',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['singleSRC']     = ['Bilddatei',"Bitte wählen Sie eine Datei aus."];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['name']       = ['Ansprechperson Name',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['firstname']       = ['Ansprechperson Vorname',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['phone']       = ['Telefonnummer',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['mobile']      = ['Handynummer',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['fax']         = ['Fax',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['email']       = ['E-Mail',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['memberid']    = ['Mitglieder Login',"Zuweisung des Eintrages an ein Frontend Member zur Bearbeitung des Eintrages."];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['website']     = ['Website',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['language']    = ['Sprache',""];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['disable']     = ['Deaktivieren',"Das Konto vorübergehend deaktivieren."];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['start']       = ['Aktiviert am',"Wenn Sie das Konto erst ab einem bestimmten Zeitpunkt aktivieren möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer. "];
$GLOBALS['TL_LANG']['tl_yellow_pages_entry']['stop']        = ['Deaktivieren am',"Wenn Sie das Konto nur bis zu einem bestimmten Zeitpunkt aktivieren möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer. "];
