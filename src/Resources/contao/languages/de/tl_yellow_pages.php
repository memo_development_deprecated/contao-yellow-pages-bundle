<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */



$GLOBALS['TL_LANG']['tl_yellow_pages']['categories_legend']   = 'Kategorien';
$GLOBALS['TL_LANG']['tl_yellow_pages']['protected_legend']   = 'Zugriffsschutz';
$GLOBALS['TL_LANG']['tl_yellow_pages']['title_legend']   = 'Archiv';

$GLOBALS['TL_LANG']['tl_yellow_pages']['title']   = ['Titel',""];
$GLOBALS['TL_LANG']['tl_yellow_pages']['categories']   = ['Kategorie Zuordnung',"Welche Kategorien sollen zur Verfügung stehen?"];
$GLOBALS['TL_LANG']['tl_yellow_pages']['groups']   = ['Archiv schützen',"Inhalte nur bestimmten Benutzergruppen anzeigen?"];
$GLOBALS['TL_LANG']['tl_yellow_pages']['protected']   = ['Erlaubte Mitgliedergruppen',""];
