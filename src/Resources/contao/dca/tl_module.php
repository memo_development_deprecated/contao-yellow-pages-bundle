<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_yellowpages_register_form'] = '{title_legend}, name,type,headline;{form_legend},yellowPageArchive,buttonLabel,uploadDir,loginRedirect;{categories_legend},categorieGroups;{formfields_legend},formFields;{redirect_legend},jumpTo;{notification_legend:hide},notification;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_yellowpages_categorie_filter'] = '{title_legend}, name,type,headline;{categories_legend},categories;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_yellowpages_listing']         = '{title_legend},name,headline,type;{config_legend},yellow_pages_archives,numberOfItems,perPage,yellow_pages_order,yellow_pages_register_link,yellow_pages_register_buttonLabel;{template_legend:hide},yellow_pages_template,customTpl;{image_legend:hide},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';


// Add fields to tl_module
$GLOBALS['TL_DCA']['tl_module']['fields']['yellow_pages_archives'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'options_callback'        => array('tl_module_memo_ext', 'getYellowPagesArchives'),
    'eval'                    => array('multiple'=>true, 'mandatory'=>true),
    'sql'                     => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['yellow_pages_order'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback'        => array('tl_module_memo_ext', 'getSortingOptions'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_module'],
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(32) NOT NULL default 'order_date_desc'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['yellow_pages_template'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback' => static function ()
    {
        return Controller::getTemplateGroup('yellow_pages_');
    },
    'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
    'sql'                     => "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['yellow_pages_register_link'] = [
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'foreignKey'              => 'tl_page.title',
    'eval'                    => array('mandatory'=>false, 'fieldType'=>'radio', 'tl_class'=>'clr'),
    'sql'                     => "int(10) unsigned NOT NULL default 0",
    'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
];

$GLOBALS['TL_DCA']['tl_module']['fields']['yellow_pages_register_buttonLabel'] = [
    'exclude'                 => true,
    'inputType'               => 'text',
    'default'                 => 'Eintrag{{br}}hinzufügen',
    'eval'                    => array('tl_class'=>'w50','mandatory'=>true),
    'sql'                     => "varchar(255) NULL default ''"
];


$GLOBALS['TL_DCA']['tl_module']['fields']['categories'] = [
    'exclude' 					=> true,
    'filter'					=> false,
    'inputType'					=> 'checkbox',
    'foreignKey'				=> 'tl_memo_category.title',
    'options_callback'			=> ['memo.foundation.category', 'getAllCategories'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];
$GLOBALS['TL_DCA']['tl_module']['fields']['categorieGroups'] = [
    'exclude' 					=> true,
    'filter'					=> false,
    'inputType'					=> 'checkbox',
    'foreignKey'				=> 'tl_memo_category.title',
    'options_callback'			=> ['memo.foundation.category', 'getCategoryGroups'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['formFields'] = [
    'exclude' 					=> true,
    'filter'					=> false,
    'inputType'					=> 'checkboxWizard',
    'foreignKey'				=> 'tl_memo_category.title',
    'options_callback'			=> ['tl_module_memo_ext', 'getDcaFields'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['yellowPageArchive'] = [
    'exclude' 					=> true,
    'filter'					=> false,
    'inputType'					=> 'radio',
    'options_callback'        => array('tl_module_memo_ext', 'getYellowPagesArchives'),
    'eval'						=> array(
        'multiple' => false,
        'chosen' => true,
        'mandatory'=>true,
        'tl_class' => 'w50 m12'
    ),
    'sql'						=> "blob NULL"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['notification'] = [
    'exclude'					=> true,
    'inputType'					=> 'select',
    'eval'						=> array('mandatory'=>false, 'tl_class'=>'w50 clr','includeBlankOption'=>true, 'multiple'=>false, 'chosen'=>true),
    'foreignKey'			    => 'tl_nc_notification.title',
    'sql'						=> "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['uploadDir'] = [
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('fieldType'=>'radio', 'tl_class'=>'w50 clr','mandatory'=>true),
    'sql'                     => "binary(16) NULL"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['loginRedirect'] = [
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'eval'                    => array('fieldType'=>'radio', 'tl_class'=>'w50','mandatory'=>true),
    'sql'                     => "binary(16) NULL"
];
$GLOBALS['TL_DCA']['tl_module']['fields']['buttonLabel'] = [
    'exclude'                 => true,
    'inputType'               => 'text',
    'default'                 => 'Eintrag speichern',
    'eval'                    => array('tl_class'=>'w50','mandatory'=>true),
    'sql'                     => "varchar(255) NULL default ''"
];




class tl_module_memo_ext extends Backend
{

    /**
     * Generate Option Callback with DCA fields for frontend Output
     * only eval feEditable = true
     */
    public function getDcaFields($dc) {
        \Controller::loadDataContainer('tl_yellow_pages_entry');
        \System::loadLanguageFile('tl_yellow_pages_entry');

        $aReturn = [];
        foreach($GLOBALS['TL_DCA']['tl_yellow_pages_entry']['fields'] as $key => $val) {

            if(isset($val['eval']['feEditable']) and $val['eval']['feEditable'] == true)
            {
                $aReturn[$key] = $GLOBALS['TL_LANG']['tl_yellow_pages_entry'][$key][0];
            }
        }
        return($aReturn);
    }

    /**
     * Return the sorting options
     *
     * @param DataContainer $dc
     *
     * @return array
     */
    public function getSortingOptions(DataContainer $dc)
    {
        return array('order_dateAdded_asc', 'order_dateAdded_desc', 'order_company_asc', 'order_company_desc', 'order_random');
    }

    /**
     * Get all Yellow Pages Archives and return them as array
     *
     * @return array
     */
    public function getYellowPagesArchives()
    {
        $arrArchives = array();
        $objArchives = $this->Database->execute("SELECT id, title FROM tl_yellow_pages ORDER BY title");
        $security = System::getContainer()->get('security.helper');

        while ($objArchives->next())
        {
            $arrArchives[$objArchives->id] = $objArchives->title;
        }

        return $arrArchives;
    }

}
