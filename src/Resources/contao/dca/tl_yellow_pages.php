<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPagesBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\DataContainer;
use Memo\CategoryBundle\Service\CategoryService;

$GLOBALS['TL_DCA']['tl_yellow_pages'] = array
(
    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'ctable'                      => array('tl_yellow_pages_entry'),
        'switchToEdit'                => true,
        'enableVersioning'            => true,
        'markAsCopy'                  => 'title',
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 1,
            'fields'                  => array('title'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'href'                => 'table=tl_yellow_pages_entry',
                'icon'                => 'edit.svg'
            ),
            'editheader' => array
            (
                'href'                => 'act=edit',
                'icon'                => 'header.svg',
            ),
            'copy' => array
            (
                'href'                => 'act=copy',
                'icon'                => 'copy.svg',
            ),
            'delete' => array
            (
                'href'                => 'act=delete',
                'icon'                => 'delete.svg',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ),
            'show' => array
            (
                'href'                => 'act=show',
                'icon'                => 'show.svg'
            )
        )
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('protected', 'allowComments'),
        'default'                     => '{title_legend},title;{categories_legend},categories;{protected_legend:hide},protected;'
    ),

    // Subpalettes
    'subpalettes' => array
    (
        'protected'                   => 'groups',
        'allowComments'               => 'notify,sortOrder,perPage,moderate,bbcode,requireLogin,disableCaptcha'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default 0"
        ),
        'title' => array
        (
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'jumpTo' => array
        (
            'exclude'                 => true,
            'inputType'               => 'pageTree',
            'foreignKey'              => 'tl_page.title',
            'eval'                    => array('mandatory'=>true, 'fieldType'=>'radio', 'tl_class'=>'clr'),
            'sql'                     => "int(10) unsigned NOT NULL default 0",
            'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
        ),
        'protected' => array
        (
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'groups' => array
        (
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'foreignKey'              => 'tl_member_group.name',
            'eval'                    => array('mandatory'=>true, 'multiple'=>true),
            'sql'                     => "blob NULL",
            'relation'                => array('type'=>'hasMany', 'load'=>'lazy')
        ),
        'categories' => array
        (
            'exclude' 					=> true,
            'search'					=> false,
            'inputType'					=> 'checkbox',
            'foreignKey'				=> 'tl_memo_category.title',
            'options_callback'			=> ['tl_yellow_pages', 'getEventCategories'],
            'eval'						=> array(
                'multiple' => true,
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'clr long'
            ),
            'sql'						=> "blob NULL"
        ),
        'notify' => array
        (
            'exclude'                 => true,
            'inputType'               => 'select',
            'options'                 => array('notify_admin', 'notify_author', 'notify_both'),
            'eval'                    => array('tl_class'=>'w50'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_yellow_pages'],
            'sql'                     => "varchar(32) NOT NULL default 'notify_admin'"
        ),
        'sortOrder' => array
        (
            'exclude'                 => true,
            'inputType'               => 'select',
            'options'                 => array('ascending', 'descending'),
            'reference'               => &$GLOBALS['TL_LANG']['MSC'],
            'eval'                    => array('tl_class'=>'w50 clr'),
            'sql'                     => "varchar(32) NOT NULL default 'ascending'"
        ),
        'perPage' => array
        (
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'natural', 'tl_class'=>'w50'),
            'sql'                     => "smallint(5) unsigned NOT NULL default 0"
        ),
        'moderate' => array
        (
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'bbcode' => array
        (
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'requireLogin' => array
        (
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'disableCaptcha' => array
        (
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        )
    )
);



/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @property Archive $Archive
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class tl_yellow_pages extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import(BackendUser::class, 'User');
    }


    /**
     * @return mixed
     */
    public function getEventCategories(){
        //only Category Parent ID 1
        return CategoryService::getCategoryGroups();
    }

}
