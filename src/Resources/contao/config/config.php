<?php

/**
 * Contao Open Source CMS
 *
 * @package   YellowPagesBundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
$GLOBALS['BE_MOD']['memo_yellowpages']['tl_yellow_pages'] = array(
    'tables' => array(
        'tl_yellow_pages',
        'tl_yellow_pages_entry'
    )
);


/**
 * Add front end modules
 */
$GLOBALS['FE_MOD']['memo_yellowpages'] = array
(
    'memo_yellowpages_register_form'    => 'Memo\YellowPagesBundle\Module\ModuleYellowPageRegisterForm',
    'memo_yellowpages_categorie_filter'    => 'Memo\YellowPagesBundle\Module\ModuleYellowPageKategorieFilter',
    'memo_yellowpages_listing'    => 'Memo\YellowPagesBundle\Module\ModuleYellowPageListing'
);

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_yellow_pages']                    = 'Memo\YellowPagesBundle\Model\YellowPagesModel';
$GLOBALS['TL_MODELS']['tl_yellow_pages_entry']              = 'Memo\YellowPagesBundle\Model\YellowPagesEntryModel';

/**
 * HOOKS
 */


// Style sheet
if (defined('TL_MODE') && TL_MODE == 'BE')
{
    $GLOBALS['TL_CSS'][] = 'bundles/yellowpages/css/yellow-pages-backend.css|static';
}
