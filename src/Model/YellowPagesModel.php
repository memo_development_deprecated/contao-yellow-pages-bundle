<?php

namespace Memo\YellowPagesBundle\Model;

use Contao\Model;

/**
 * Class YellowPagesModel
 *
 * Reads and writes tl_yellow_pages Data.
 */

class YellowPagesModel extends Model
{
    protected static $strTable = 'tl_yellow_pages';
}
