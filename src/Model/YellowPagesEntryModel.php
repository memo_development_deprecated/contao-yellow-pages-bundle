<?php

namespace Memo\YellowPagesBundle\Model;

use Contao\Model;

/**
 * Class YellowPagesEntryModel
 *
 * Reads and writes tl_yellow_pages_entry Data.
 */

class YellowPagesEntryModel extends Model
{
    protected static $strTable = 'tl_yellow_pages_entry';


    /**
     * Count published items by their parent ID
     *
     * @param array   $arrPids     An array of YellowPage archive IDs
     * @param array   $arrOptions  An optional options array
     *
     * @return integer The number of news items
     */
    public static function countPublishedByPids($arrPids, $blnFeatured=null, array $arrOptions=array())
    {
        if (empty($arrPids) || !\is_array($arrPids))
        {
            return 0;
        }

        $t = static::$strTable;
        $arrColumns = array("$t.pid IN(" . implode(',', array_map('\intval', $arrPids)) . ")");

        if (!static::isPreviewMode($arrOptions))
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "$t.disable != '1' AND ($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'$time')";
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }


    /**
     * @param array $arrPids
     * @param int $intLimit
     * @param int $intOffset
     * @param array $arrOptions
     * @return Model|Model[]|Model\Collection|YellowPagesEntryModel|null
     */
    public static function findPublishedByPids(array $arrPids,int $intLimit=0, int $intOffset=0, array $arrOptions=array())
    {
        if (empty($arrPids) || !\is_array($arrPids))
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = array("$t.pid IN(" . implode(',', array_map('\intval', $arrPids)) . ")");

        // Never return unpublished elements in the back end, so they don't end up in the RSS feed
        if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "$t.disable != '1' AND ($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'$time')";
        }

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order']  = "$t.dateAdded DESC";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }

}
